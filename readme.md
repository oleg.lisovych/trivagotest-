<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## How to run it!

1. git clone git@gitlab.com:oleg.lisovych/trivagotest-.git
2. sudo chmod -R 777 trivagotest-/*
3.  composer install
4. create  database
5. copy .env.example and create your own .env file with your DB settings
6. php artisan key:generate
7. php artisan migrate

It was my first experience with Laravel but I decided to try.

<div>Import and Export Data Into Excel and CSV in Laravel 5</div>


{!! Form::open(array('route' => 'hotel.import','method'=>'POST','files'=>'true')) !!}
@if (Session::has('success'))
    <div>{{ Session::get('success') }}</div>
@elseif (Session::has('warnning'))
    <div>{{ Session::get('warnning') }}</div>
@endif
{!! Form::label('sample_file','Select File to Import:',['class'=>'col-md-3']) !!}
{!! Form::file('hotels', array('class' => 'form-control')) !!}
{!! $errors->first('hotels', '<p class="alert alert-danger">:message</p>') !!}

{!! Form::submit('Upload',['class'=>'btn btn-success']) !!}

{!! Form::close() !!}


<br/><br/>

<a href="{{ route('hotel.export',['type'=>'xls']) }}" class="btn btn-primary" style="margin-right: 15px;">Download -
    Excel xls</a>
<a href="{{ route('hotel.export',['type'=>'xlsx']) }}" class="btn btn-primary" style="margin-right: 15px;">Download -
    Excel xlsx</a>
<a href="{{ route('hotel.export',['type'=>'csv']) }}" class="btn btn-primary" style="margin-right: 15px;">Download -
    CSV</a>

<br/><br/>
<table width="100%" border="1" cellpadding="10">
    <tr>
        <th>Name</th>
        <th>Address</th>
        <th>Stars</th>
        <th>Contact</th>
        <th>Phone</th>
        <th>Uri</th>
    </tr>

    @foreach($hotels as $hotel)
        <tr>
            <td>{{$hotel->name}}</td>
            <td>{{$hotel->address}}</td>
            <td>{{$hotel->stars}}</td>
            <td>{{$hotel->contact}}</td>
            <td>{{$hotel->phone}}</td>
            <td>{{$hotel->uri}}</td>
        </tr>
    @endforeach

</table>

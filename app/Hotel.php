<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name','address','stars','contact','phone','uri',
    ];
}

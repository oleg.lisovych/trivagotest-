<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Hotel;
use Excel;

class HotelController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(){
        $hotels = Hotel::get();
        return view('hotels', compact('hotels'));
    }

    /**
     * @param $value
     * @return string|string[]|null
     */
    public function validateHotelName($value)
    {
        //looks for all non-printable characters and remove them.
        if(!empty($value)) {
            return preg_replace('/[[:^print:]]/', '', $value);
        }
        else {
            return $value = "-";
        }
    }

    /**
     * @param $url
     * @return mixed|string
     */
    public function validateHotelUrl($url)
    {
        // Remove all illegal characters from a url
        $url = filter_var($url, FILTER_SANITIZE_URL);

        // Validate url
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            return $url;
        } else {
            return $url = '-';
        }
    }

    /**
     * @param int $value
     * @return int
     */
    public function validateHotelStars(int $value)
    {
        if($value >= 0 && $value <= 5) {
            return $value;
        }
        else
            return $value = 0;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function hotelsImport(Request $request){
        if($request->hasFile('hotels')){
            $path = $request->file('hotels')->getRealPath();
            $rules = [
                'hotels' => 'required|mimes:csv,txt'
            ];
            $this->validate($request, $rules);
            $data = \Excel::load($path)->get();
            if($data->count()){

                foreach ($data as $key => $value) {
                    //print_r($value);
                    $hotels_list[] = [
                        'name' => $this->validateHotelName($value->name),
                        'address' => !empty($value->address) ? $value->address : '',
                        'stars' => $this->validateHotelStars($value->stars),
                        'contact' => !empty($value->contact) ? $value->contact : '-',
                        'phone' => !empty($value->phone) ? $value->phone : '-',
                        'uri' => $this->validateHotelUrl($value->uri)
                    ];
                }

                if(!empty($hotels_list)){
                    Hotel::insert($hotels_list);
                    \Session::flash('success','File improted successfully.');
                }
            }
        }else{
            \Session::flash('warnning','There is no file to import');
        }
        return Redirect::back();
    }

    /**
     * @param $type
     * @return mixed
     */
    public function hotelsExport($type){
        $hotels = Hotel::select('name','address','stars','contact','phone','uri')->get()->toArray();
        return \Excel::create('Hotels', function($excel) use ($hotels) {
            $excel->sheet('Hotel Details', function($sheet) use ($hotels)
            {
                $sheet->fromArray($hotels);
            });
        })->download($type);
    }}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HotelController@list')->name('hotel.list');
Route::post('hotel-import', 'HotelController@hotelsImport')->name('hotel.import');
Route::get('hotel-export/{type}', 'HotelController@hotelsExport')->name('hotel.export');

